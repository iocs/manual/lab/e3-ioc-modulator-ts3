require modbus
require essioc
require modulator

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "TS3-010:SC-IOC-001")
epicsEnvSet(IOCDIR, "TS3-010_SC-IOC-001")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# Load MODULATOR module
############################################################################
epicsEnvSet(SYS, "TS3")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "001")
epicsEnvSet(MOD_IP, "172.30.5.202")
#-epicsEnvSet(PSS_PV, "FEB-010Row:CnPw-U-004:ToRFQLPS")
iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

############################################################################
# IOC Startup
############################################################################
iocInit()

